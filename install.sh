#!/bin/bash
INSTALL_DIR="/opt/just-build"
while [[ $# > 0  ]]
do
    KEY=$1
    case $KEY in
        -t|--target)
            INSTALL_DIR=$2
            if [[ -e $INSTALL_DIR ]]
            then
                if ! [[ -d $INSTALL_DIR ]]
                then
                    echo "ERROR: $INSTALL_DIR is not directory!"
                    exit 1
                fi
            else
                echo "$INSTALL_DIR not existing, creating..."
                sudo mkdir -p $INSTALL_DIR
            fi
            shift
        ;;
        *)
            echo "ERROR: Unrecognised param: $1"
            exit 2
        ;;
    esac
    shift
done
echo "Installing just-build to $INSTALL_DIR"

sudo mkdir -p $INSTALL_DIR
sudo cp src/jb.sh $INSTALL_DIR
sudo cp src/jbset.sh $INSTALL_DIR
sudo cp src/jbclean.sh $INSTALL_DIR
sudo update-alternatives --install /usr/bin/jb jb $INSTALL_DIR/jb.sh 1
sudo update-alternatives --install /usr/bin/jbclean jbclean $INSTALL_DIR/jbclean.sh 1
sudo update-alternatives --install /usr/bin/jbset jbset $INSTALL_DIR/jbset.sh 1
