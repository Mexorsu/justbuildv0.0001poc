#!/bin/bash
INSTALL_DIR=""
while [[ $# > 0  ]]
do
    KEY=$1
    case $KEY in
        -t|--target)
            INSTALL_DIR=$2
            if [[ -e $INSTALL_DIR ]]
            then
                if ! [[ -d $INSTALL_DIR ]]
                then
                    echo "ERROR: $INSTALL_DIR is not directory!"
                    exit 1
                fi
            else
                echo "$INSTALL_DIR not existing, creating..."
                sudo mkdir -p $INSTALL_DIR
            fi
            shift
        ;;
        *)
            echo "ERROR: Unrecognised param: $1"
            exit 2
        ;;
    esac
    shift
done
if [[ $INSTALL_DIR == "" ]]
then
    INSTALL_DIR=`sudo update-alternatives --list jb 2>/dev/null | sed -e 's#^\(.*\)/\([^/]\+\)$#\1#' | head -n 1`
fi
if [[ $INSTALL_DIR == "" ]]
then
    echo "No installation found, or installation corrupted. Specify install folder by running with -t <INSTALLATION DIRECTORY> argument to try and force remove"
    exit 3
fi
echo "Removing just-build from $INSTALL_DIR"
echo "* uninstalling alternatives"
sudo update-alternatives --remove jb $INSTALL_DIR/jb.sh
sudo update-alternatives --remove jbclean $INSTALL_DIR/jbclean.sh
sudo update-alternatives --remove jbset $INSTALL_DIR/jbset.sh
echo "* removing binaries"
sudo rm -rf $INSTALL_DIR
echo "--- done ---"
