#!/bin/bash

function before_build() {
    DEFAULT_BEFORE_BUILD=1
}

function after_setup() {
    DEFAULT_AFTER_SETUP=1
}

function after_build() {
    DEFAULT_AFTER_BUILD=1
}

function join_dir_list(){
    DATA=$1
    PREFIX=${2:=""}
    POSTFIX=${3:=""}
    for LINE in "${DATA[@]}"
    do
        if ! [[ -d $LINE ]]
        then
            echo_warning "directory \"$LINE\" not found"
        else
            cd $LINE
            THE_PATH=`pwd`
            cd - 1>/dev/null
            RESULT="${RESULT}${PREFIX}${THE_PATH}${POSTFIX}"
        fi
    done

}

function join_list(){
    DATA=$1
    PREFIX=$2
    POSTFIX=$3
    for LINE in "${DATA[@]}"
    do
        RESULT="${RESULT}${PREFIX}${LINE}${POSTFIX}"
    done
    echo $RESULT
}
function addSrcInclude() {
    if [[ $1 != "" ]]
    then
        INCLUDES_STRING="$INCLUDES_STRING -name '$1'"
    fi
}
function addSrcExclude(){
    if [[ $1 != "" ]]
    then
        EXCLUDES_STRING="$EXCLUDES_STRING -not -name '$1'"
    fi
}
function addSrcDir(){
    if ! [[ -d $1 ]]
    then
        error "Not a directory: $1"
    else
        SRC_PATHS_STRING="$SRC_PATHS_STRING $1"
    fi
}
function addIncludePath(){
    if ! [[ -d $1 ]]
    then
        error "Not a directory: $1"
    else
        INCLUDE_PATHS_STRING="${INCLUDE_PATHS_STRING} -I$1"
    fi
}
function addLibraryPath(){
    if ! [[ -d $1 ]]
    then
        error "Not a directory: $1"
    else
        LIBRARY_PATHS_STRING="${LIBRARY_PATHS_STRING} -L$1"
    fi
}
function addFlag(){
    if [[ $1 != "" ]]
    then
        FLAGS_STRING="${FLAGS_STRING} $1"
    fi
}
function addSrcFile(){
    if ! [[ -f $1 ]]
    then
        error "Not a file: $1"
    else
        echo $1 >> .jb/files_list
    fi
}
function addLib(){
    LIBS_STRING="${LIBS_STRING} -l$1"
}

JB_MODE_TEMP=$JB_MODE
JB_MODE="BUILD"
. jbset "$@"

echo_header "Parsing settings.sh"
for INCLUDE in "${SRC_INCLUDES[@]}"
do
    addSrcInclude $INCLUDE
done
for EXCLUDE in "${SRC_EXCLUDES[@]}"
do  
    addSrcExclude $EXCLUDE
done
for SRC_PATH in "${SOURCE_DIRS[@]}"
do
    if ! [[ -d $SRC_PATH ]]
    then
        echo_warning "source path \"$SRC_PATH\" not found"
    else
        cd $SRC_PATH
        SRC_PATH=`pwd`
        cd - 1>/dev/null
        addSrcDir $SRC_PATH
    fi
done
for INCLUDE_PATH in "${INCLUDE_DIRS[@]}"
do
    if ! [[ -d $INCLUDE_PATH ]]
    then
        echo_warning "include path \"$INCLUDE_PATH\" not found"
    else
        cd $INCLUDE_PATH
        INCLUDE_PATH=`pwd`
        cd - 1>/dev/null
        addIncludePath $INCLUDE_PATH
    fi
done
for LIBRARY_PATH in "${LIBRARY_DIRS[@]}"
do
    if ! [[ -d $LIBRARY_PATH ]]
    then
        echo_warning "library path \"$LIBRARY_PATH\" not found"
    else
        cd $LIBRARY_PATH
        LIBRARY_PATH=`pwd`
        cd - 1>/dev/null
        addLibraryPath $LIBRARY_PATH
    fi
done
for FLAG in "${FLAGS[@]}"
do
    addFlag $FLAG
done
for LIB in "${LIBS[@]}"
do
    addLib $LIB
done
FILESEARCH_CMD="find${SRC_PATHS_STRING}${INCLUDES_STRING}${EXCLUDES_STRING}"
if ! [[ -d $OUTPUT_DIR ]]
then
    echo_warning "directory \"$OUTPUT_DIR\" does not exist, creating"
    mkdir -p $OUTPUT_DIR
fi
cd $OUTPUT_DIR
OUTPUT_DIR=`pwd`
if ! [[ -d .jb ]]
then
    mkdir .jb
fi
bash -c "$FILESEARCH_CMD > .jb/files_list"
echo_header "Running \"before_build()\""
before_build
echo_header "Configuration loaded"
echo_verbose "Includes:        ${INCLUDES_STRING}"
echo_verbose "Excludes:        ${EXCLUDES_STRING}"
echo_verbose "SrcPaths:        ${SRC_PATHS_STRING}"
echo_verbose "IncludePaths:    ${INCLUDE_PATHS_STRING}"
echo_verbose "LibraryPaths:    ${LIBRARY_PATHS_STRING}"
echo_verbose "Flags:           ${FLAGS_STRING}"
echo_verbose "Libs:            ${LIBS_STRING}"
echo_header "Finding sources"
echo_verbose "$FILESEARCH_CMD"
echo_verbose "Sources:"
while read FILE_TO_BUILD
do
    echo_verbose "  $FILE_TO_BUILD"
done < .jb/files_list
echo_header "Building $APP_NAME as $TARGET in $OUTPUT_DIR"
if [[ $TARGET == "EXECUTABLE" ]]
then
    BUILD_CMD="${COMPILER_CMD}${FLAGS_STRING}${INCLUDE_PATHS_STRING}${LIBS_STRING}${LIBRARY_PATHS_STRING} -c"
    LINK_CMD="${COMPILER_CMD}${FLAGS_STRING} -o $APP_NAME"
    ARTIFACT="${OUTPUT_DIR}/${APP_NAME}"
elif [[ $TARGET == "STATIC LIBRARY" ]]
then
    BUILD_CMD="${COMPILER_CMD}${FLAGS_STRING}${INCLUDE_PATHS_STRING}${LIBS_STRING}${LIBRARY_PATHS_STRING} -c"
    LINK_CMD="ar rcs lib${APP_NAME}.a "
    ARTIFACT="${OUTPUT_DIR}/lib${APP_NAME}.a"
fi
# run the build cmd
rm .jb/outs 2>/dev/null
while read FILE_TO_COMPILE
do
    THE_FILE=`echo $FILE_TO_COMPILE | sed -e 's#.*/\([^/]\+\)$#\1#'`
    OUT_FILE=`echo $THE_FILE | sed -e 's#\(.*\)\.[^.]\+$#\1.obj#'`
    OUT_FILE="${OUTPUT_DIR}/${OUT_FILE}"
    if [[ $OUT_FILE -nt $FILE_TO_COMPILE ]]
    then
          echo_verbose "No changes in $FILE_TO_COMPILE since last build, skipping.."
    else
        echo_verbose "${BUILD_CMD} ${FILE_TO_COMPILE} -o $OUT_FILE"
        bash -c "${BUILD_CMD} ${FILE_TO_COMPILE} -o $OUT_FILE"
    fi
    echo "$OUT_FILE" >> .jb/outs
done < .jb/files_list
ONE_LINE_FILES_LIST=`cat .jb/outs | xargs echo`
echo_verbose "${LINK_CMD} ${ONE_LINE_FILES_LIST}${LIBS_STRING}${LIBRARY_PATHS_STRING}"
if [[ $TARGET == "EXECUTABLE" ]]
then
    bash -c "${LINK_CMD} ${ONE_LINE_FILES_LIST}${LIBS_STRING}${LIBRARY_PATHS_STRING}"
elif [[ $TARGET == "STATIC LIBRARY" ]]
then
    bash -c "cat .jb/outs | xargs $LINK_CMD"
fi
echo_header "Running \"after_build()\""
after_build
cd - 1>/dev/null
JB_MODE=$JB_MODE_TEMP
echo_header "All done"
