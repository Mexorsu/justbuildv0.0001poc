#!/bin/bash

function custom_clean() {
    NO_CUSTOM_CLEAN=1
}

JB_MODE_TEMP=$JB_MODE
JB_MODE="CLEAN"
. jbset "$@"
echo_header "Cleaning $APP_NAME"
echo_verbose "rm -rf $OUTPUT_DIR"
rm -rf $OUTPUT_DIR
custom_clean
JB_MODE=$JB_MODE_TEMP
echo_header "All done"
