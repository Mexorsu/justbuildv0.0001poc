#!/bin/bash

error(){
    echo "  ERROR: $1"
    exit 1
}

echo_header(){
    TEXT=$1
    TEXT_LEN=${#TEXT}
    COLUMNS=$(tput cols)
    SPACER=$((COLUMNS-TEXT_LEN))
    NUM=$((SPACER/2-1))
    echo
    v=$(printf "%-${NUM}s $1 %-${NUM}s" "-" "-")
    echo "${v// /-}"
    echo
     
}

echo_info(){
    COLUMNS=$(tput cols) 
    TEXT="***  $1  ***"
    printf "%*s\n" $(((${#TEXT}+$COLUMNS)/2)) "$TEXT"
}

echo_warning(){
    echo "  WARNING: $1"
}

echo_verbose() {
    if [[ $VERBOSE -eq 1 ]]
    then
        echo "  VERBOSE: $1"
    fi
}

get_dir_path(){
    if [[ -d $1 ]]
    then
        cd $1
        echo `pwd`
        cd -
    else
        error "\"$1\" is not a directory"
    fi
}

validate_settings(){
    VALIDATION_STATE="ok"
    if [[ "$APP_NAME" == "" ]]
    then
        VALIDATION_STATE="No APP_NAME set. Please set a valid APP_NAME"
    fi
    if [[ "$TARGET" == "" ]] && [[ $JB_MODE != "CLEAN" ]]
    then
        VALIDATION_STATE="No TARGET set. Please set valid TARGET eiter in settings.sh file or by passing appropriate arg to jb command (-s|-e|-d)"
    elif [[ $TARGET != "EXECUTABLE" ]] && [[ $TARGET != "STATIC LIBRARY" ]] && [[ $TARGET != "DYNAMIC LIBRARY" ]] && [[ $TARGET != "" ]]
    then
        VALIDATION_STATE="Invalid TARGET \"$TARGET\" Please choose one of the following targets: \"EXECUTABLE\", \"STATIC LIBRARY\" or \"DYNAMIC LIBRARY\" "
    fi 
    if [[ $VALIDATION_STATE != "ok" ]]
    then
        error "While validating $BUILD_DIR/settings.sh: $VALIDATION_STATE"
    fi
}

VERBOSE=0
BUILD_DIR="."
# parse arguments
while [[ $# > 0  ]]
do
    KEY=$1
    case $KEY in
        -e|--exec)
            TARGET="EXECUTABLE"
        ;;
        -s|--static)
            echo "settng target "
            TARGET="STATIC LIBRARY"
        ;;
        -d|--dynamic)
            TARGET="DYNAMIC LIBRARY"
            error "Not yet supported"
        ;;
        -v|--verbose)
            VERBOSE=1
        ;;
        *)
            BUILD_DIR=$KEY
        ;;
    esac
    shift
done

# discover settings.sh
SETTINGS_COUNT=`find $BUILD_DIR -maxdepth 1 -name 'settings.sh' | wc -l`
if [[ $SETTINGS_COUNT -gt 1 ]]
then
    error "Invalid configuration: multiple settings.sh detected in \"${BUILD_DIR}\"."
elif
    [[ "$SETTINGS_COUNT" == "0" ]]
then
    error "Invalid configuration settings.sh was not found in \"${BUILD_DIR}\"."
fi

# Load some defaults
COMPILER_CMD="g++"
OUTPUT_DIR="bin"
FLAGS=()
LIBS=()
SOURCE_DIRS=(".")
INCLUDE_DIRS=()
LIBRARY_DIRS=()
SRC_INCLUDES=("*.cpp")
SRC_EXCLUDES=()
# Enter build dir, load and validate settings
cd $BUILD_DIR
BUILD_DIR=`pwd`
. settings.sh 2>/dev/null
validate_settings

