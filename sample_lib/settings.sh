#!/bin/bash
APP_NAME="SampleLib"
COMPILER_CMD="g++"
OUTPUT_DIR="lib"
FLAGS=("-std=c++11" "-Wall")
LIBS=("pthread")
SOURCE_DIRS=("src")
INCLUDE_DIRS=()
LIBRARY_DIRS=()
SRC_INCLUDES=("*.cpp")
SRC_EXCLUDES=()
after_build(){
    ARTIFACT_FILENAME=`echo $ARTIFACT | sed -e 's#.*/\([^/]\+\)$#\1#'`
    rm -rf ../../sample/lib/$ARTIFACT_FILENAME
    mkdir -p ../../sample/lib
    echo "Copying $ARTIFACT_FILENAME to ../../sample/lib"
    cp $ARTIFACT ../../sample/lib
    echo "Copying headers to $BUILD_DIR/../sample/additional_headers"
    find ../ -name *.h -exec cp {} $BUILD_DIR/../sample/additional_headers \;
}

