Disclaimer
==========

This is an early (and poorly hacked in bash) proof of concept of JustBuild, multi-platform, multi-language build automation tool. For a real, java version check out https://bitbucket.org/mexorsu/justbuild

JustBuild
=========

This is a simple bash "app" which allows for easy automation of building C/C++ projects.

It uses settings.sh files to define build variables such as library paths, include
directories, libs, include/exclude patterns, flags, compilers, etc. Allows easy integration
of custom build steps.

Installation:
-------------

```bash
  $ git clone http://bitbucket.org/mexorsu/justbuild just-build
  $ cd just-build
  $ ./install.sh
```
Uninstallation:
---------------

```bash
  $ git clone http://bitbucket.org/mexorsu/justbuild just-build
  $ cd just-build
  $ ./uninstall.sh
```
Basic usage:
-----------

To build an executable:

```bash
  $ jb -e <PATH>
```
To build static library:

```bash
  $ jb -s <PATH>
```
To build dynamic library (not supported, check out hhtps://bitbucket.org/mexorsu/justbuild):

```bash
  $ jb -d <PATH>
```
Where <PATH> is an optional argument referencing path to settings.sh file, which contains build information;

Example settings.sh files can be found in the repo.

Custom build steps:
-------------------
At this point custom build steps can be introduces by ovveriding before_build, after_build and custom_clean methods.


More fancy stuff to come, hopefully.