#!/bin/bash
echo "#### Running Sanity Check ####"
jbclean sample_lib && jbclean sample && jb -s sample_lib && jb -e sample && BUILD_RESULT="OK"
APP_RESULT=`./sample/binary/SampleApp`
if [[ $BUILD_RESULT == "OK" && $APP_RESULT == "OK" ]]
then
    echo "#### Sanity Check PASSED ####"
else
    echo "#### Sanity Check FAILED ####"
fi
