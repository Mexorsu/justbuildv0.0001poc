#!/bin/bash
APP_NAME="SampleApp"
COMPILER_CMD="g++"
OUTPUT_DIR="binary"
FLAGS=("-std=c++11" "-Wall")
LIBS=("pthread" "SampleLib")
SOURCE_DIRS=("src")
INCLUDE_DIRS=("additional_headers")
LIBRARY_DIRS=("lib")
SRC_INCLUDES=("*.cpp")
SRC_EXCLUDES=("NotFinished.cpp")
custom_clean(){
    rm -rf $BUILD_DIR/additional_headers/SomeLib.h
    rm -rf $BUILD_DIR/lib/libSampleLib.a
}
